#!/usr/bin/env bash

#fastq to fasta 
if (($(wc -l < $1)%4!=0))  #checks if the file contains a number of lines that is a multiple of 4
then 
    echo 'ERROR: invalid number of rows'
else 
    awk '{if((NR%4==1)||(NR%4==2)) print}' $1 | sed 's/^@/>/g' #t keeps only the first two lines out of four and then replaces the '@' sign at the beginning of the line with '>'
fi

