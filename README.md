# qTOa Tools 

## Table of Contents
1. [What is qTOa?](#What is qTOa?)
2. [Folder's contents ](#folder's contents)
3. [Installation on a local galaxy instance](#installation)
4. [Usage](#Usage)

## What is qTOa ? 
qTOa is a parser tool. It allows you to convert a file in fastq format (.fastq) into a file in fasta format (.fa / .fasta )
 
The code checks if the file contains a number of lines that is a multiple of 4.

If it does, it keeps only the first two lines out of four and then replaces the '@' sign at the beginning of the line with '>'.

## Folder's contents 
In this folder there are :
- the tool script ( qTOa.sh )
- the xml script (qTOa.xml) . It will allow the installation of the tool in the galaxy instance.
- the README ( README.md ) that you are currently reading. It contains the classical information about the tool and how to use it. 
- The documentation (qTOa_documentation.txt) containing the technical part of the tool. 
- the LICENSE file.
- a folder (test_files) containing the different test files for the verification of the good functioning of the tool. 

## Installation on a local galaxy instance

Currently you are inside the "qTOa_tools" folder (a changer ). You normally have on your machine the folder "galaxy" of your instance containing a folder "tools" with your tools inside. 

- The first step is to create a "qTOa" folder in your galaxy/tools folder. To do this : 

**mkdir -p path/galaxy/tools/qTOa**

Be careful to replace path by the path to your folder.

(Tip : if you don't know how to get a path, go to the folder from the terminal and use the pwd command. Remember to come back later in the "qTOa_tools" folder)

- The second step is to add the files qTOa.xml and qTOa.sh in the folder galaxy/tools/qTOa 

/!\ be careful to always be in the "qTOa_tools" folder

**cp qTOa.sh qTOa.xml path/galaxy/tools/qTOa**

/!\ check if qTOa.sh has executable permission  :

**cd path/galaxy/tools/qTOa**

**ls -l** 

you must have '-rwxr-xr-x'at the beginning of the line of qTOa.sh, if you don't have the 'x', do :

**chmod a+x qTOa.sh**


- The third step is to modify the configuration script of galaxy. For that, go to the "config" folder

**cd path/galaxy/config**

open the tool_conf.xml file. Go to the end of the file and add :
```
 <section name="qTOa : fastq to fasta" id="qTOa_parser_fastq_to_fasta_id">
    <tool file="qTOa/qTOa.xml" />
 </section>

``` 
/!\ make sure that the identations correspond to the other sections in the file 
Save and refresh (or restart) your galaxy instance. You should see qTOa appear! Congratulations :-) 


## Usage 

### 1. Use from a terminal
./qTOa.sh name_input.fastq > name_output.fasta

example using test file:

from the folder with the qTOa.sh script and the test_file folder :  
```
./qTOa.sh test_file/test_100_seq.fastq > test_file/test_100_seq.fasta
```
The new fasta file is in test_file folder. 

### 2. Use from the galaxy instance

In your instance, download the fastq files you want to convert, click on qTOa . 

With qTOa in your galaxy instance, you can convert up to 4 fastq files at once. You just have to choose the number of fastq you want to enter (between 1 and 4) then select the chosen files in the list of your download files ( ctrl + left click).

